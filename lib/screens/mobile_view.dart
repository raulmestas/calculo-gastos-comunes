import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MobileView extends StatefulWidget {
  const MobileView({super.key});

  @override
  State<MobileView> createState() => _MobileViewState();
}

class _MobileViewState extends State<MobileView> {
  String _ingreso1 = "";
  double _dIngreso1 = 0.0;
  String _ingreso2 = "";
  double _dIngreso2 = 0.0;
  String _ingresoTotal = "";
  double _dIngresoTotal = 0.0;
  double _porcentaje1 = 0.0;
  double _porcentaje2 = 0.0;
  double _cantidad1 = 0.0;
  double _cantidad2 = 0.0;
  String _porcentaje1string = "";
  String _porcentaje2string = "";
  String _cantidad1string = "";
  String _cantidad2string = "";

  void _incrementCounter() {
    setState(() {
      _dIngreso1 = double.parse(_ingreso1);
      _dIngreso2 = double.parse(_ingreso2);
      _dIngresoTotal = double.parse(_ingresoTotal);

      _porcentaje1 = (_dIngreso1 / (_dIngreso1 + _dIngreso2));
      _porcentaje2 = (_dIngreso2 / (_dIngreso1 + _dIngreso2));

      _cantidad1 = _dIngresoTotal * _porcentaje1;
      _cantidad1string = _cantidad1.toStringAsFixed(2);
      _cantidad2 = _dIngresoTotal * _porcentaje2;
      _cantidad2string = _cantidad2.toStringAsFixed(2);

      _porcentaje1 *= 100;
      _porcentaje1string = _porcentaje1.toStringAsFixed(2);
      _porcentaje2 *= 100;
      _porcentaje2string = _porcentaje2.toStringAsFixed(2);

      // Guardado de forma local de los datos
      addCantidades(_dIngreso1, _dIngreso2, _dIngresoTotal);
    });
  }

  Future addCantidades(
      double datoIng1, double datoIng2, double datoTotal) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble('datoIng1', datoIng1);
    prefs.setDouble('datoIng2', datoIng2);
    prefs.setDouble('datoTotal', datoTotal);
  }

  @override
  void initState() {
    intiTextFields();
    super.initState();
  }

  Future intiTextFields() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _dIngreso1 = await prefs.getDouble("datoIng1") ?? 1000.0;
    _dIngreso2 = await prefs.getDouble("datoIng2") ?? 1200.0;
    _dIngresoTotal = await prefs.getDouble("datoTotal") ?? 800.0;

    _ingreso1 = _dIngreso1.toString();
    _ingreso2 = _dIngreso2.toString();
    _ingresoTotal = _dIngresoTotal.toString();

    _incrementCounter();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("------ mobile view ------"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              children: [
                Text(
                  "Salario 1:",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                Expanded(
                  child: TextField(
                    onChanged: ((value) => _ingreso1 = value),
                    controller: TextEditingController(text: _ingreso1),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text("Salario 2:"),
                Expanded(
                  child: TextField(
                    onChanged: ((value) => _ingreso2 = value),
                    controller: TextEditingController(text: _ingreso2),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text("Ingreso:"),
                Expanded(
                  child: TextField(
                    onChanged: ((value) => _ingresoTotal = value),
                    controller: TextEditingController(text: _ingresoTotal),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Text("Porcentaje 1: "),
                    Text("$_porcentaje1string%"),
                    Text("Cantidad 1: "),
                    Text("$_cantidad1string€")
                  ],
                ),
                Row(
                  children: [
                    Text("Porcentaje 2: "),
                    Text("$_porcentaje2string%"),
                    Text("Cantidad 2: "),
                    Text("$_cantidad2string€")
                  ],
                ),
              ],
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
